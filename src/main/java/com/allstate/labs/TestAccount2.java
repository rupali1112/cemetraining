package com.allstate.labs;

public class TestAccount2 {
    public static void main(String[] args) {
        
        Account[] arrayOfAccounts = new Account[5];
        arrayOfAccounts[0]= new Account("Rupali", 10000.0);
        arrayOfAccounts[1]= new Account("Data", 2);
        arrayOfAccounts[2]= new Account("Worf", 256);
        arrayOfAccounts[3]= new Account("Troy", 345);
        arrayOfAccounts[4]= new Account("Ryker", 134);
    
        Account.setInterestRate(10);

        for (Account account : arrayOfAccounts) {
            System.out.println("Name: " + account.getName()
            +"  Account Balance: "+account.getBalance());
            account.addInterest();
            System.out.println("Balance with Interest: "+ account.getBalance());
           
        }
      //System.out.println( arrayOfAccounts[0].withdraw(11000));
      if(arrayOfAccounts[1].withdraw())
      System.out.println( "Transaction successful and Updated Balance is: "+ arrayOfAccounts[1].getBalance());
      else  System.out.println( "Transaction failed due to insufficient Balance is: "+ arrayOfAccounts[1].getBalance());


       }
}
