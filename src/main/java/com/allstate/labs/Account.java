package com.allstate.labs;

public class Account {
    
    private String name;
    private double balance;
    private static double interestRate; 
    

    public Account(){

    }

    public Account(String name, double balance)
    {
        this.name=name;
        this.balance=balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }
    
public void addInterest(){
        this.balance+=interestRate*this.balance/100;
    }
public boolean withdraw(double amount){
    if(this.balance-amount>=0){
        this.balance=this.balance-amount;
         return true;
    }
    else  return false;
    
}
public boolean withdraw(){
    return withdraw(20);
}
}
