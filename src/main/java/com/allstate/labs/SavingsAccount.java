package com.allstate.labs;

public class SavingsAccount extends Account {

    private String name;
    private double balance;
    
    public SavingsAccount() {
    }

    public SavingsAccount(String name, double balance) {
        super(name, balance);
        this.name = name;
        this.balance = balance;
    }

    @Override
    public void addInterest(){
    
        setBalance(getBalance()*1.4);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
}
