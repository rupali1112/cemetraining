package com.allstate.labs;

public class TestInheritance {
    public static void main(String[] args) {
        
        Account[] arrayOfAccounts = new Account[3];
        arrayOfAccounts[0]= new Account("Rupali", 2);
        arrayOfAccounts[1]= new CurrentAccount("John", 4);
        arrayOfAccounts[2]= new SavingsAccount("Barry", 6);

        Account.setInterestRate(10);

        for (Account account : arrayOfAccounts) {
            System.out.println("Name: " + account.getName()
            +"  Account Balance: "+account.getBalance());
            account.addInterest();
            System.out.println("Balance with Interest: "+ account.getBalance());
           
        }
    }

}
