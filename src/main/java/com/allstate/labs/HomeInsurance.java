package com.allstate.labs;

public class HomeInsurance implements Detailable {
    private double amountInsured;
    private double excess;
   private double premium;

   @Override
   public String getDetails() {
       // TODO Auto-generated method stub
       return ""+ premium +" "+ excess;
   }

   public double getAmountInsured() {
       return amountInsured;
   }

   public void setAmountInsured(double amountInsured) {
       this.amountInsured = amountInsured;
   }

   public double getExcess() {
       return excess;
   }

   public void setExcess(double excess) {
       this.excess = excess;
   }

   public double getPremium() {
       return premium;
   }

   public void setPremium(double premium) {
       this.premium = premium;
   }
    
}
