package com.allstate.springlab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ComponentScan("com.allstate")
public class EngineConfig {

    @Bean
    public Car car(@Autowired Engine engine) {
      return new Car(engine);
    }

    @Bean
    public Engine engine() {
    return new DieselEngine();
    } 

 
  
    
}
