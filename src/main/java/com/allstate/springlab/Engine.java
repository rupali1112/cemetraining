package com.allstate.springlab;

public interface Engine {
    double getEngineSize();
     void setEngineSize(double engineSize);
    
}
