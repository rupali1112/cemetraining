package com.allstate.entities;

public class Person {
    private int id;
    private String name;
    private int age;
    private final static int ageLimit=100;

    public Person(){

        this.id=0;
    }

    public Person(int id){

        this.id=id;
    }
    
    public Person(int id, String name,  int age)
    {
        this.name=name;
        this.id=id;
        this.age=age;
    

    }

    public int getID(){
        return this.id;
    } 
    public void setId(int id){
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static int getAgeLimit() {
        return ageLimit;
    }

public void display(){

    System.out.printf("Value are %s, %d, %d" , this.name, this.id, this.age);
}

protected void print(){

    System.out.printf("Value are %s, %d, %d" , this.name, this.id, this.age);
}

void printFormat(){

    System.out.printf("Value are %s, %d, %d" , this.name, this.id, this.age);
}



}
